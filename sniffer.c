#include <stdio.h>
#include <stdlib.h>
#include <pcap.h> 
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <net/ethernet.h>
#include <netinet/ether.h> 
#include <netinet/ip.h>
#include <string.h>


/*Prototypes*/
void clean_stdin(void);
void sniffer(char *dev, char *errbuf);
void my_callback(u_char *args, const struct pcap_pkthdr *hdr, const u_char *packet);
u_int16_t handle_ethernet(u_char *args, const struct pcap_pkthdr *hdr,const u_char *packet);
u_char* handle_IP(u_char *args, const struct pcap_pkthdr *hdr,const u_char *packet);
int set_filter(char * buf);
int handle_login_item_s(char* addr);
int handle_login_item_d(char* addr);
void write_log();
void cleanup();

/*User define structures*/
#define LOGIN_ITEM_STRING_SIZE 20
#define LOGIN_ITEMS_SIZE 100
#define FILTER_SIZE 100
#define PACKETS_NUMBER_LENGTH 20
#define ADDRESS_LENGTH 20
#define TCP_PACKET 6
#define UDP_PACKET 17
#define IP_V4 4
#define IP_HEADER_LENGTH 5

struct Logitem
{
    char addr_string[LOGIN_ITEM_STRING_SIZE];
    unsigned int packets_number;
};

struct my_ip 
{
        u_int8_t ip_vhl;                        /* header length, version */
#define IP_V(ip) (((ip)->ip_vhl & 0xf0) >> 4) 
#define IP_HL(ip) ((ip)->ip_vhl & 0x0f)
        u_int8_t ip_tos;                        /* type of service */
        u_int16_t ip_len;                       /* total length */
        u_int16_t ip_id;                        /* identification */
        u_int16_t ip_off;                       /* fragment offset field */
#define IP_DF 0x4000                            /* dont fragment flag */
#define IP_MF 0x2000                            /* more fragments flag */
#define IP_OFFMASK 0x1fff                       /* mask for fragmenting bits */
        u_int8_t ip_ttl;                        /* time to live */
        u_int8_t ip_p;                          /* protocol */
        u_int16_t ip_sum;                       /* checksum */
        struct in_addr ip_src,ip_dst;           /* source and dest address */
};

/*Globals*/
u_int tcp_pack_count = 0;
u_int udp_pack_count = 0;
size_t count;
char *filter;
struct Logitem* items[LOGIN_ITEMS_SIZE] = {0};


int main(int argc, char *argv[])
{

    char ch;
    char *dev;
    char errbuf[PCAP_ERRBUF_SIZE] =  {0};

    fprintf(stderr, "\nHello\, this is a simple sniffer\, choice what do you want to do:\n\n\n");
        
    do{
        fprintf(stderr, "\t1) Observe the list of network interfaces\n");
        fprintf(stderr, "\t2) Enter the network interface you want to listen and start capturing\n");
        fprintf(stderr, "\t3) Exit\n");

        ch = getchar();
        clean_stdin();
                
        switch(ch)
        {
            case '1':
                system("ifconfig");
                break;
            case '2':
                printf("Enter interface:");
                scanf("%s", dev);
      
                if(dev == NULL)
                {
                    printf("%s\n",errbuf);
                    exit(1);
                } 
      
                printf("Selected network interface: %s\n", dev);
          
                sniffer(dev, errbuf);
                break;
            case '3':
                exit(1);
                break;
            default:
                fprintf(stderr, "Type 1 or 2 or 3\n");
                break;
        }
    } while(1);
        
    return(0);
}

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

void sniffer(char *dev, char *errbuf)
{
    pcap_t* descr;
    struct bpf_program fp;
    bpf_u_int32 netp;
    bpf_u_int32 maskp;
    struct pcap_pkthdr hdr;
    u_char* args = NULL;
            
    char filter[FILTER_SIZE] ={0};
	set_filter(filter);
        
	pcap_lookupnet(dev,&netp,&maskp,errbuf);

    descr = pcap_open_live(dev,BUFSIZ,1,1000,errbuf);
        
    if(descr == NULL)
    {
        printf("pcap_open_live(): %s\n",errbuf);
        exit(1);
    }

    if(pcap_compile(descr,&fp,filter,0,netp) == -1)
    {
        fprintf(stderr,"Error calling pcap_compile\n");
        exit(1);
    }

    if(pcap_setfilter(descr,&fp) == -1)
    {
        fprintf(stderr,"Error setting filter\n");
        exit(1);
    } 
        
    pcap_loop(descr,atoi("100"),my_callback,args);
    fprintf(stdout,"\nDone processing packets!\n");

    write_log();
    cleanup();
          
}

void my_callback(u_char *args, const struct pcap_pkthdr *hdr, const u_char *packet)
{
        
    u_int16_t type = handle_ethernet(args,hdr,packet);
        
    if(ntohs(type) == ETHERTYPE_IP)
    {
        u_char* un = handle_IP(args,hdr,packet);
    }
    else
    {
        fprintf(stderr, "We can't handle that package\n");
    } 
} 

u_int16_t handle_ethernet(u_char *args,const struct pcap_pkthdr* hdr,const u_char *packet)
{
    struct ether_header *eptr; 
    
    eptr = (struct ether_header *) packet;
    
    fprintf(stdout,"\nethernet header source: %s",
        ether_ntoa(eptr->ether_shost));
    fprintf(stdout," destination: %s ",
        ether_ntoa(eptr->ether_dhost));

    handle_login_item_s(ether_ntoa(eptr->ether_shost));
    handle_login_item_d(ether_ntoa(eptr->ether_dhost));

    if (ntohs (eptr->ether_type) == ETHERTYPE_IP)
    {
        fprintf(stdout,"(This is IP packet)");
    }else if (ntohs (eptr->ether_type) == ETHERTYPE_ARP)
    {
        fprintf(stdout,"(This is ARP packet)");
    }else if (ntohs (eptr->ether_type) == ETHERTYPE_REVARP)
    {
        fprintf(stdout,"(This is RARP packet)");
    }else {
        fprintf(stdout,"(?)");
    }
        fprintf(stdout,"\n");
        return eptr->ether_type;
}

u_char* handle_IP(u_char *args,const struct pcap_pkthdr* hdr,const u_char* packet)
{
    const struct my_ip* ip;
    u_int version, hlen; 
    u_int8_t er;
        
    ip = (struct my_ip*)(packet + sizeof(struct ether_header));
        
    hlen = IP_HL(ip); /* header length */
    version = IP_V(ip);/* ip version */ 
    er = (ip->ip_p);
        
    if(version != IP_V4)
    {
        fprintf(stdout,"Unknown version %d\n",version);
        return NULL;
    } 

    if(hlen < IP_HEADER_LENGTH)
    {
        fprintf(stdout,"bad-hlen %d \n",hlen);
    }

    fprintf(stderr, "SOURCE IP: %s\t", inet_ntoa(ip->ip_src));
    fprintf(stderr, "DESTINATION IP: %s\t", inet_ntoa(ip->ip_dst));
        
    handle_login_item_s(inet_ntoa(ip->ip_src));
    handle_login_item_d(inet_ntoa(ip->ip_dst));

    if(er == TCP_PACKET)
    {
        fprintf(stderr, "This is TCP packet #%d", ++tcp_pack_count);            
    }else if(er == UDP_PACKET)
    {
        fprintf(stderr, "This is UDP packet #%d", ++udp_pack_count);
    }

    return NULL;
}  

int set_filter(char* buf)
{

    fprintf(stdout, "Set the filter for capturing of the packages\n");
    fprintf(stdout, "for example - src/dst host 192.168.2.235 - for capturing packages with source/destination IP 192.168.2.235 or - src/dst port 80 - for capturing packages with source/destination 80 port or - icmp - for capturing only icmp or pres enter for no filers\n");
	
    clean_stdin();
    fgets(buf,FILTER_SIZE,stdin);
	
    return 0;
}

int handle_login_item_s(char* addr)
{
    int i;
    for (i = 0; i < LOGIN_ITEMS_SIZE/2; i++)
    {
        if (items[i] != NULL)
        {
            if (strcmp(items[i]->addr_string, addr)==0)
            {
                items[i]->packets_number++;
                return 0;
            }
        }
        else
        {
            items[i] = (struct Logitem*)calloc(1, sizeof(struct Logitem));
            if (items[i] != NULL)
            {
                strcpy(items[i]->addr_string, addr);
                items[i]->packets_number = 1;
                return 0;
            }
            else
            {
                // TODO Print smthing
                return 1;
            }

        }
    }
}

int handle_login_item_d(char* addr)
{
    int i;
    for (i = LOGIN_ITEMS_SIZE/2; i < LOGIN_ITEMS_SIZE; i++)
    {
        if (items[i] != NULL)
        {
            if (strcmp(items[i]->addr_string, addr)==0)
            {
                items[i]->packets_number++;
                return 0;
            }
        }
        else
        {
            items[i] = (struct Logitem*)calloc(1, sizeof(struct Logitem));
            if (items[i] != NULL)
            {
                strcpy(items[i]->addr_string, addr);
                items[i]->packets_number = 1;
                return 0;
            }
            else
            {
                // TODO Print smthing
                return 1;
            }

        }
    }
}

void write_log()
{
    FILE *stream;

    stream = fopen("log", "w");

    if(!stream) 
    {
        perror("can't open file");
        return EXIT_FAILURE;
    }

    int i;
    char p[PACKETS_NUMBER_LENGTH];
    char tcp[ADDRESS_LENGTH];
    char udp[ADDRESS_LENGTH];

    for (i = 0; i < LOGIN_ITEMS_SIZE/2; i++)
    {
        if (items[i] != NULL)
        {
            snprintf(p, PACKETS_NUMBER_LENGTH, "%d", items[i]->packets_number);
            if(fputs(p, stream) == EOF)
                perror("can't write data in file");
            if(fputs(" packages send from ", stream) == EOF)
                perror("can't write data in file");
            if(fputs(strncat(items[i]->addr_string,"\n",1), stream) == EOF)
                perror("can't write data in file");
        }
    }

    for (i = LOGIN_ITEMS_SIZE/2; i < LOGIN_ITEMS_SIZE; i++)
    {
        if (items[i] != NULL)
        {
            snprintf(p, PACKETS_NUMBER_LENGTH, "%d", items[i]->packets_number);
            if(fputs(p, stream) == EOF)
                perror("can't write data in file");
            if(fputs(" packages send to ", stream) == EOF)
                perror("can't write data in file");
            if(fputs(strncat(items[i]->addr_string,"\n",1), stream) == EOF)
                perror("can't write data in file");
        }
    }

    snprintf(tcp, ADDRESS_LENGTH, "%d", tcp_pack_count);
    if(fputs("Quantity of TCP packages ", stream) == EOF)
        perror("can't write data in file");
    if(fputs(tcp, stream) == EOF)
        perror("can't write data in file");
    if(fputs("\n", stream) == EOF)
        perror("can't write data in file");

    snprintf(udp, ADDRESS_LENGTH, "%d", udp_pack_count);
    if(fputs("Quantity of UDP packages ", stream) == EOF)
        perror("can't write data in file");
    if(fputs(udp, stream) == EOF)
        perror("can't write data in file");
    
    fclose(stream);
}

void cleanup()
{
    int i;
    for (i = 0; i < LOGIN_ITEMS_SIZE; i++)
    {
        if (items[i] != NULL)
        {
            free(items[i]);
        }
    }
}