cc=gcc

CFLAGS=-lpcap -o

all:
	$(cc) sniffer.c $(CFLAGS) sniffer 

clean:
	rm -f *.out sniffer log sniffer

install:
	cp sniffer /usr/local/bin
uninstall:
	rm -f /usr/local/bin/sniffer
